package org.nuiton.config.plugin.templates;

/*-
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.plugin.model.ConfigModel;

/**
 * Created on 01/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public interface ApplicationConfigTransformerConfig {

    ConfigModel getConfigModel();

    String getModelName();

    String getPackageName();

    String getConfigClassName();

    boolean isGenerateProvider();

    boolean isGeneratePropertyChangeSupport();

    boolean isUseNuitonI18n();

    String getConfigProviderClassName();

    String getOptionsClassName();

    String getActionsClassName();

}
