package org.nuiton.config.plugin.model;

/*-
 * #%L
 * Nuiton Config :: Maven plugin
 * %%
 * Copyright (C) 2016 Code Lutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.config.ConfigActionDef;
import org.nuiton.eugene.GeneratorUtil;

/**
 * Created on 01/10/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class ActionModel {

    private String name;
    private String description;
    private String action;
    private String[] aliases;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String[] getAliases() {
        return aliases;
    }

    public void setAliases(String... aliases) {
        this.aliases = aliases;
    }

    public static ActionModel of(ConfigActionDef configActionDef) {
        ActionModel actionModel = new ActionModel();
        actionModel.setAction(configActionDef.getAction());
        actionModel.setAliases(configActionDef.getAliases());
        actionModel.setDescription(configActionDef.getDescription());

        if (configActionDef instanceof Enum) {
            Enum optionDef = (Enum) configActionDef;
            actionModel.setName(GeneratorUtil.convertConstantNameToVariableName(optionDef.name()));
        }
        return actionModel;
    }
}
