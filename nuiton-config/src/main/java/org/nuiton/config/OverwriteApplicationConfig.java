package org.nuiton.config;

/*
 * #%L
 * Nuiton Config :: API
 * %%
 * Copyright (C) 2016 Code Lutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */


import java.util.Map;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Permet d'avoir une liste de configuration dans lequel on va chercher les valeurs
 * avant de regarder dans les valeurs de la configuration
 *
 * @see ApplicationConfig#getConfig(Map)
 *
 * @author poussin
 */
public class OverwriteApplicationConfig extends ApplicationConfig {

    /** to use log facility, just put in your code: log.info(\"...\"); */
    static private Log log = LogFactory.getLog(OverwriteApplicationConfig.class);

    protected ApplicationConfig parent;
    protected Map<String, String> overwrite;

    public OverwriteApplicationConfig(ApplicationConfig parent, Map<String, String> overwrite) {
        this.parent = parent;
        this.overwrite = overwrite;
    }

    @Override
    protected void init(ApplicationConfigInit init) {
        // do nothing
    }

    public ApplicationConfig getParent() {
        return parent;
    }

    @Override
    public String getOption(String key) {
        String value = overwrite.get(key);
        // on est oblige de faire un get, car le containsKey n'est pas recursif
        // sur tous les properties si Map est un Properties

        if (value != null) {
            value = overwrite.get(key);
            // replace ${xxx}
            value = replaceRecursiveOptions(value);
        } else {
            value = parent.getOption(key);
        }
        
        return value;
    }

    @Override
    public boolean hasOption(String key) {
        boolean result = getOption(key) != null;
        return result;
    }

    // methode interdite dans le sub
    @Override
    public ApplicationConfig parse(String... args) throws ArgumentsParserException {
        throw new UnsupportedOperationException("This method is not supported in OverwriteApplicationConfig");
    }
    
}
